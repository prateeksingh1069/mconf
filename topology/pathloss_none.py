#!/usr/bin/env python
from emanesh.events import EventService
from emanesh.events import CommEffectEvent
from emanesh.events import LocationEvent
from emanesh.events import PathlossEvent

# create the event service
service = EventService(('224.1.2.8',45703,'emanenode0'))

# pathloss


# 0% loss
# create an event setting the pathloss between 1 & 10
event = PathlossEvent()
event.append(1,forward=0)
event.append(2,forward=0)
event.append(3,forward=0)
event.append(4,forward=0)
event.append(5,forward=0)
event.append(6,forward=0)
event.append(7,forward=0)
event.append(8,forward=0)
event.append(9,forward=0)
event.append(10,forward=0)
# publish the event
service.publish(1,event)
service.publish(2,event)
service.publish(3,event)
service.publish(4,event)
service.publish(5,event)
service.publish(6,event)
service.publish(7,event)
service.publish(8,event)
service.publish(9,event)
service.publish(10,event)
