#!/usr/bin/env python

import sys
import time
from emanesh.events import EventService
from emanesh.events import PathlossEvent

delta = 5
if (len(sys.argv) == 2):
    delta = int(sys.argv[1])
print 'using delta=' + str(delta)

# create the event service
service = EventService(('224.1.2.8',45703,'emanenode0'))

# create an event setting the pathloss between nodes
event = PathlossEvent()
event.append(10,forward=0)
event.append(1,forward=0)
# publish the event
service.publish(10,event)
service.publish(1,event)

event = PathlossEvent()
event.append(10,forward=0)
event.append(3,forward=0)
service.publish(10,event)
service.publish(3,event)

event = PathlossEvent()
event.append(10,forward=0)
event.append(2,forward=0)
service.publish(10,event)
service.publish(2,event)

event = PathlossEvent()
event.append(1,forward=0)
event.append(4,forward=0)
service.publish(1,event)
service.publish(4,event)

event = PathlossEvent()
event.append(3,forward=0)
event.append(5,forward=0)
service.publish(3,event)
service.publish(5,event)

event = PathlossEvent()
event.append(2,forward=0)
event.append(4,forward=0)
service.publish(2,event)
service.publish(4,event)

event = PathlossEvent()
event.append(2,forward=0)
event.append(5,forward=0)
service.publish(2,event)
service.publish(5,event)

event = PathlossEvent()
event.append(4,forward=0)
event.append(7,forward=0)
service.publish(4,event)
service.publish(7,event)

event = PathlossEvent()
event.append(5,forward=0)
event.append(7,forward=0)
service.publish(5,event)
service.publish(7,event)

event = PathlossEvent()
event.append(4,forward=0)
event.append(6,forward=0)
service.publish(4,event)
service.publish(6,event)

event = PathlossEvent()
event.append(5,forward=0)
event.append(8,forward=0)
service.publish(5,event)
service.publish(8,event)

event = PathlossEvent()
event.append(7,forward=0)
event.append(9,forward=0)
service.publish(7,event)
service.publish(9,event)

i = 1
#for change in range(1,1000):
while i == 1:
	time.sleep(delta)

	event = PathlossEvent()
	event.append(8,forward=0)
	event.append(9,forward=0)
	service.publish(8,event)
	service.publish(9,event)

	time.sleep(delta)

	event = PathlossEvent()
	event.append(7,forward=300)
	event.append(9,forward=300)
	service.publish(7,event)
	service.publish(9,event)

	time.sleep(delta)

	event = PathlossEvent()
	event.append(7,forward=0)
	event.append(9,forward=0)
	service.publish(7,event)
	service.publish(9,event)

	time.sleep(delta)

	event = PathlossEvent()
	event.append(8,forward=300)
	event.append(9,forward=300)
	service.publish(8,event)
	service.publish(9,event)

#sleep, topology change
#time.sleep(10)

#event = PathlossEvent()
#event.append(8,forward=0)
#event.append(9,forward=0)
#service.publish(8,event)
#service.publish(9,event)

#time.sleep(10)

#event = PathlossEvent()
#event.append(6,forward=0)
#event.append(9,forward=0)
#service.publish(6,event)
#service.publish(9,event)
