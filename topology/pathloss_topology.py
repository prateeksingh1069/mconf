#!/usr/bin/env python

import sys
import time
from emanesh.events import EventService
from emanesh.events import PathlossEvent

# topology
connections = []

if (len(sys.argv) == 2):
    topology_file = sys.argv[1]
    print 'using file ' + topology_file
    # topology
    edges = tuple(open(topology_file, 'r'))
    for edge in edges:
        nodes = edge.split(',')
        print 'edge from node ' + str(nodes[0]) + ' to node ' + str(nodes[1])
        connections.append((int(nodes[0]), int(nodes[1])));
else:
    print 'no file'
    # topology
    #connections = []
    connections.append((10, 1));
    connections.append((10, 3));
    connections.append((10, 2));
    connections.append((1, 4));
    connections.append((3, 5));
    connections.append((2, 4));
    connections.append((2, 5));
    connections.append((4, 7));
    connections.append((5, 7));
    connections.append((4, 6));
    connections.append((5, 8));
    connections.append((7, 9));

# create the event service
service = EventService(('224.1.2.8',45703,'emanenode0'))

# topology
#connections = []
#connections.append((10, 1));
#connections.append((10, 3));
#connections.append((10, 2));
#connections.append((1, 4));
#connections.append((3, 5));
#connections.append((2, 4));
#connections.append((2, 5));
#connections.append((4, 7));
#connections.append((5, 7));
#connections.append((4, 6));
#connections.append((5, 8));
#connections.append((7, 9));

# create an event setting the pathloss between nodes
for connection in connections:
	nodeA = connection[0]
	nodeB = connection[1]	
	print str(nodeA) + ' to ' + str(nodeB)
	event = PathlossEvent()
	event.append(nodeA,forward=0)
	event.append(nodeB,forward=0)
	service.publish(nodeA,event)
	service.publish(nodeB,event)

#sleep, topology change
#time.sleep(10)

#event = PathlossEvent()
#event.append(8,forward=0)
#event.append(9,forward=0)
#service.publish(8,event)
#service.publish(9,event)

#time.sleep(10)

#event = PathlossEvent()
#event.append(6,forward=0)
#event.append(9,forward=0)
#service.publish(6,event)
#service.publish(9,event)
