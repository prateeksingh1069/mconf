#!/usr/bin/env python
import sys
from emanesh.events import EventService
from emanesh.events import CommEffectEvent
from emanesh.events import LocationEvent
from emanesh.events import PathlossEvent

if (len(sys.argv) < 2):
    print("Usage: commeffect_loss.py <loss%>")
    exit(0)

# create the event service
service = EventService(('224.1.2.8',45703,'emanenode0'))

# set bitrate and loss
bitrate_value = 100000000
loss_value = float(sys.argv[1])

print 'setting loss to ' + str(loss_value) + '%'

event = CommEffectEvent()
event.append(1,loss=loss_value,latency=0,jitter=0,duplicate=0,unicast=bitrate_value,broadcast=bitrate_value)
event.append(2,loss=loss_value,latency=0,jitter=0,duplicate=0,unicast=bitrate_value,broadcast=bitrate_value)
event.append(3,loss=loss_value,latency=0,jitter=0,duplicate=0,unicast=bitrate_value,broadcast=bitrate_value)
event.append(4,loss=loss_value,latency=0,jitter=0,duplicate=0,unicast=bitrate_value,broadcast=bitrate_value)
event.append(5,loss=loss_value,latency=0,jitter=0,duplicate=0,unicast=bitrate_value,broadcast=bitrate_value)
event.append(6,loss=loss_value,latency=0,jitter=0,duplicate=0,unicast=bitrate_value,broadcast=bitrate_value)
event.append(7,loss=loss_value,latency=0,jitter=0,duplicate=0,unicast=bitrate_value,broadcast=bitrate_value)
event.append(8,loss=loss_value,latency=0,jitter=0,duplicate=0,unicast=bitrate_value,broadcast=bitrate_value)
event.append(9,loss=loss_value,latency=0,jitter=0,duplicate=0,unicast=bitrate_value,broadcast=bitrate_value)
event.append(10,loss=loss_value,latency=0,jitter=0,duplicate=0,unicast=bitrate_value,broadcast=bitrate_value)
service.publish(1,event)
service.publish(2,event)
service.publish(3,event)
service.publish(4,event)
service.publish(5,event)
service.publish(6,event)
service.publish(7,event)
service.publish(8,event)
service.publish(9,event)
service.publish(10,event)

