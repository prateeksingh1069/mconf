#!/bin/bash

echo 'SETTING UP TESTING ENVIRONMENT'
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

#BASIC DEPENDENCIES
sudo apt-get -y install build-essential
sudo apt-get -y install python

#HOSTS
echo 'updating hosts file'
sudo -- sh -c "echo '10.99.0.1 node-1' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.2 node-2' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.3 node-3' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.4 node-4' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.5 node-5' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.6 node-6' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.7 node-7' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.8 node-8' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.9 node-9' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.10 node-10' >> /etc/hosts"
sudo -- sh -c "echo '10.99.0.100 node-server' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.1 radio-1' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.2 radio-2' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.3 radio-3' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.4 radio-4' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.5 radio-5' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.6 radio-6' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.7 radio-7' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.8 radio-8' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.9 radio-9' >> /etc/hosts"
sudo -- sh -c "echo '10.100.0.10 radio-10' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.1 n1' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.2 n2' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.3 n3' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.4 n4' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.5 n5' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.6 n6' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.7 n7' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.8 n8' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.9 n9' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.10 n10' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.11 n11' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.12 n12' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.13 n13' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.14 n14' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.15 n15' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.16 n16' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.17 n17' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.18 n18' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.19 n19' >> /etc/hosts"
sudo -- sh -c "echo '10.200.0.20 n20' >> /etc/hosts"

#SSH CONFIG (ignore host key changes for CORE)
mkdir ~/.ssh
sudo -- sh -c "echo $'HOST n1\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n2\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n3\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n4\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n5\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n6\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n7\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n8\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n9\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n10\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n11\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n12\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n13\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n14\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n15\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n16\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n17\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n18\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n19\n\tStrictHostKeyChecking no' >> ~/.ssh/config"
sudo -- sh -c "echo $'HOST n20\n\tStrictHostKeyChecking no' >> ~/.ssh/config"

#CONF
echo 'setting up CONF'
sudo apt-get -y install terminator
sudo apt-get -y install python-dev
sudo easy_install netifaces

#R
echo 'installing R'
sudo apt-get -y install r-base r-base-dev
echo 'installing ggplot2'
sudo R --file=$DIR/setup/ggplot.R

#PERMISSIONS
echo 'setting permissions'
sudo $DIR/sudoers.sh $USER

#BonnMotion
#http://sys.cs.uos.de/bonnmotion/faq.shtml
echo 'building BonnMotion'
cd $DIR/setup/bonnmotion-2.1.2 && ./install
#cd $DIR/setup/bonnmotion-2.1.2/bin && ./bm

echo 'FINISHED'

#SSH KEY
echo 'NOTE: to finish setup perform the following steps'
echo '(or add the appropriate lines above to SSH CONFIG):'
echo 'ssh-keygen if no key exists'
echo 'with terminal in demos/zconf, sudo ./demo-start'
echo 'ssh-copy-id node-1, yes, enter password'
echo 'for nodes 2-10, ssh node-*, yes, exit'
echo 'with terminal in demos/zconf, sudo ./demo-stop'
