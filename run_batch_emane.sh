#!/bin/bash

#parameters from command line
if [ -z "$1" ]; then
	echo usage: $0 'nodes iterations loss[0,5,10,15,20,25,50,100] config[{a,b,c,d}/{none,small,medium,large}]'
	exit
else
	NODES=$1
fi

LOSS=0

if [ -z "$2" ]; then
	echo "ERROR: missing iterations"
	exit
else
	ITERATIONS=$2
fi

if [ -z "$3" ]; then
	echo "ERROR: missing loss"
	exit
else
	LOSS=$3
fi

if [ -z "$4" ]; then
	echo "ERROR: missing config"
	exit
else
	CONFIGNAME="emane/$4"
fi

MODE="$(echo $CONFIGNAME | cut -d'/' -f 2)"

if [ $MODE = "a" ]; then
	CONDITION="unconditional"
	CONFIRMATION="false"
elif [ $MODE = "b" ]; then
	CONDITION="unconditional"
	CONFIRMATION="true"
elif [ $MODE = "c" ]; then
	CONDITION="conditional"
	CONFIRMATION="false"
elif [ $MODE = "d" ]; then
	CONDITION="conditional"
	CONFIRMATION="true"
else
	echo "ERROR: unknown mode: $MODE"
	exit
fi

CONFIG="CONF/config/$CONFIGNAME.cfg"
PLATFORM="$(echo $CONFIGNAME | cut -d'/' -f 1)"
CFG="$(echo $CONFIGNAME | cut -d'/' -f 3)"
TYPE="$(echo $CONDITION | head -c 1)"
CONF="$(echo $CONFIRMATION | head -c 1)"
SIZE="$(echo $CONFIGNAME | cut -d'/' -f 3 | head -c 1)"

DIVISOR=3
DELAY=5

echo "BATCH RUN"
echo "Iterations: $ITERATIONS"
echo "Loss: $LOSS%"
echo "Platform: $PLATFORM"
echo "Nodes: $NODES"
echo "Mode: $MODE"
echo "Condition: $CONDITION"
echo "Confirmation: $CONFIRMATION"
echo "Configuration: $CFG"

REGULAR=$(($NODES - 1))

#run for N iterations
for ITER in $(eval echo {1..$ITERATIONS}); do
	echo "Iteration" $ITER of $ITERATIONS

	echo "Loading $REGULAR agents:"
	#start agents and allow a bit of time
	for ID in $(seq 1 $REGULAR); do 
		#ssh -n -t -t node-$ID "sudo ./CONF/conf_$CONDITION.py $ID $CONFIG" > /dev/null 2>&1 & 
		ssh -n -t -t node-$ID "sudo ./CONF/conf.py $ID $CONFIG" > /dev/null 2>&1 & 
	done

	#wait for nodes to start
	sleep $DELAY

	#check running nodes
	NODECOUNT=$(ps ax | grep '[c]onf.py' | wc -l)
	NODECOUNT=$(($NODECOUNT / $DIVISOR))
	echo "$NODECOUNT running nodes"
	COUNTER=10
	until [ $NODECOUNT -ge $REGULAR ]; do
		echo "WARNING: MISSING NODES, $NODECOUNT running, $COUNTER wait(s) remaining"
		if [ $COUNTER -le 0 ]; then
			read -p "hit ENTER to continue"
		fi
		sleep 4
		NODECOUNT=$(ps ax | grep '[c]onf.py' | wc -l)
		NODECOUNT=$(($NODECOUNT / $DIVISOR))
		echo "$NODECOUNT running nodes"
		let COUNTER-=1
	done
	#if [[ $NODECOUNT -lt $REGULAR ]]; then
	#	echo "WARNING: MISSING NODES"
	#	read -p "hit ENTER to continue"
	#fi

	echo "Starting ROOT:"
	#ssh -t -t node-$NODES "sudo ./CONF/conf_$CONDITION.py 0 $CONFIG" > /dev/null 2>&1
	ssh -t -t node-$NODES "sudo ./CONF/conf.py 0 $CONFIG" > /dev/null 2>&1

	#allow more time to account for any applied delay
	sleep $DELAY
	#check running nodes
	NODECOUNT=$(ps ax | grep '[c]onf.py' | wc -l)
	NODECOUNT=$(($NODECOUNT / $DIVISOR))
	echo "$NODECOUNT running nodes"
	COUNTER=10
	until [ $NODECOUNT -eq 0 ]; do
		echo "WARNING: $NODECOUNT running nodes, $COUNTER wait(s) remaining"
		if [ $COUNTER -le 0 ]; then
			echo "killing nodes..."
			sudo kill $(ps aux | grep '[c]onf.py' | awk '{print $2}')
		fi
		sleep 4
		NODECOUNT=$(ps ax | grep '[c]onf.py' | wc -l)
		NODECOUNT=$(($NODECOUNT / $DIVISOR))
		echo "$NODECOUNT running nodes"
		let COUNTER-=1
	done
	#if [[ $NODECOUNT -gt 0 ]]; then
	#	echo "WARNING: $NODECOUNT NODES STILL RUNNING"
	#	read -p "hit ENTER to kill"
	#	sudo kill $(ps aux | grep '[c]onf.py' | awk '{print $2}')
	#fi

	echo "parsing logs"
	#TODO: check for non-zero file sizes
	python analysis.py ./ 0 $REGULAR
	mv analysis _$ITER

	#clear logs
	#rm -f logs/*
	mv logs/* _$ITER/
done

#process results
DATE=$(date +%Y%m%d%H%M%S)

OUTPUT="results_emane_$NODES"
OUTPUT+="_"
OUTPUT+=$MODE
OUTPUT+="_"
OUTPUT+=$TYPE
OUTPUT+="_"
OUTPUT+=$CONF
OUTPUT+="_"
OUTPUT+=$LOSS
OUTPUT+="_"
OUTPUT+=$SIZE
#OUTPUT+="_"
#OUTPUT+=$RETRIES
#OUTPUT+="_"
#OUTPUT+=$TIMEOUT
#OUTPUT+="_"
#OUTPUT+=$DATASIZE
#OUTPUT+="_"
#OUTPUT+=$DATE

if [ "$ITERATIONS" -eq 1 ]; then
	mv _* $OUTPUT
else
	mkdir $OUTPUT
	mv _* $OUTPUT/

	#get overall durations,total packets
	cat $OUTPUT/_*/duration.txt > $OUTPUT/durations.txt
	cat $OUTPUT/_*/totalpackets.txt > $OUTPUT/totalpackets.txt

	#get individual durations,sent packets
	cat $OUTPUT/_*/duration_node.txt > $OUTPUT/durations_node.txt
	cat $OUTPUT/_*/totalpackets_node.txt > $OUTPUT/totalpackets_node.txt

	#total executed count
	cat $OUTPUT/_*/execcount.txt > $OUTPUT/execcount.txt

	#create .csv files
	LINE="s/^/"
	LINE+=$NODES
	LINE+=","
	LINE+=$LOSS
	LINE+=","
	LINE+=$MODE
	LINE+=","
	LINE+=$CONDITION
	LINE+=","
	LINE+=$CONFIRMATION
	LINE+=","
	LINE+=$CFG
	LINE+=",/"
	sed $LINE $OUTPUT/durations.txt > durations.csv
	sed $LINE $OUTPUT/durations_node.txt > durations_node.csv
	sed $LINE $OUTPUT/totalpackets.txt > totalpackets.csv
	sed $LINE $OUTPUT/totalpackets_node.txt > totalpackets_node.csv
	sed $LINE $OUTPUT/execcount.txt > execcount.csv
	cp durations.csv $OUTPUT/durations_raw.csv
	cp durations_node.csv $OUTPUT/durations_node_raw.csv
	cp totalpackets.csv $OUTPUT/totalpackets_raw.csv
	cp totalpackets_node.csv $OUTPUT/totalpackets_node_raw.csv
	cp execcount.csv $OUTPUT/execcount_raw.csv
	echo '"nodes","loss","mode","condition","conf","size","duration"' | cat - durations.csv > temp && mv -f temp durations.csv
	echo '"nodes","loss","mode","condition","conf","size","duration"' | cat - durations_node.csv > temp && mv -f temp durations_node.csv
	echo '"nodes","loss","mode","condition","conf","size","packets"' | cat - totalpackets.csv > temp && mv -f temp totalpackets.csv
	echo '"nodes","loss","mode","condition","conf","size","packets"' | cat - totalpackets_node.csv > temp && mv -f temp totalpackets_node.csv
	echo '"nodes","loss","mode","condition","conf","size","executed"' | cat - execcount.csv > temp && mv -f temp execcount.csv

	#create plots
	R --file=plot_batch.R > /dev/null 2>&1
	mv packets* $OUTPUT/
	mv durations* $OUTPUT/
	mv execpcts* $OUTPUT/
	mv totalpackets* $OUTPUT/
	mv execcount* $OUTPUT/
fi

#move output folder
mv $OUTPUT results/

echo "Finished"

