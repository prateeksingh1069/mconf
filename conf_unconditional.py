#!/usr/bin/env python
"""
Unconditional Configuration With/Without Confirmation
support for OLSR (EMANE) and OSPF (CORE)
Prateek Singh
"""

import ConfigParser
import sys
import socket
import select
import struct
import time
import os
import subprocess
import netifaces as ni
from messages import Message

version_id = '2015-01-27'

#node states
STATE_SNIFF = 0
STATE_WAIT_COMMIT = 4
STATE_WAIT_DONE = 5
STATE_EXECUTE = 6
STATE_ABORT = 7

#message types
MESSAGE_COMMIT = 'COMMIT'
MESSAGE_DONE = 'DONE'
MESSAGE_LIST = 'LIST'

COUNT_COMMIT = 0
COUNT_DONE = 0
COUNT_LIST = 0

def get_millis():
    return int(round(time.time() * 1000))

def write_log(message):
    log.write(message)

def get_ip_address(ifname):
    return ni.ifaddresses(ifname)[ni.AF_INET][0]['addr']

def get_broadcast_address(ifname):
    return ni.ifaddresses(ifname)[ni.AF_INET][0]['broadcast']

def send_broadcast(data):
#    if debug: 
#        print 'sending broadcast: ' + data
    sock_local.sendto(data, bcast_address)

def send_unicast(data, dest_ip):
#    if debug: 
#        print 'sending unicast to ' + dest_ip + ': ' + data
    sock_local.sendto(data, (dest_ip, port))

def send_message_unicast(message_type, dest_ip, node_list, data):
    tstamp = str(get_millis())
    message_id = 0 # TODO: implement
    msg = Message(message_type, message_id, local_ip, dest_ip, tstamp, node_list, data)
    send_unicast(str(msg), dest_ip)
    write_log(tstamp + ': sent ' + msg.getLogString() + ' to ' + dest_ip + '\n')

def send_message_broadcast(message_type, node_list, data):
    tstamp = str(get_millis())
    message_id = 0 # TODO: implement
    msg = Message(message_type, message_id, local_ip, bcast_ip, tstamp, node_list, data)
    send_broadcast(str(msg))
    write_log(tstamp + ': sent ' + msg.getLogString() + ' to ' + bcast_ip + '\n')

def send_commit_unicast(dest_ip):
    global COUNT_COMMIT
    
    send_message_unicast(MESSAGE_COMMIT, dest_ip, executed_nodes, generate_data(size_commit))
    COUNT_COMMIT += 1

def send_commit_broadcast():
    global COUNT_COMMIT
    
    send_message_broadcast(MESSAGE_COMMIT, executed_nodes, generate_data(size_commit))
    COUNT_COMMIT += 1

def send_done_unicast(dest_ip):
    global COUNT_DONE
    
    send_message_unicast(MESSAGE_DONE, dest_ip, executed_nodes, generate_data(size_done))
    COUNT_DONE += 1

def send_done_broadcast():
    global COUNT_DONE
    
    send_message_broadcast(MESSAGE_DONE, executed_nodes, generate_data(size_done))
    COUNT_DONE += 1

def send_list_unicast(dest_ip):
    global COUNT_LIST
    
    print 'INFO: LIST messages disabled'
    #send_message_unicast(MESSAGE_LIST, dest_ip, all_nodes, generate_data(size_list))
    COUNT_LIST += 1

def send_list_broadcast():
    global COUNT_LIST
    
    print 'INFO: LIST messages disabled'
    #send_message_broadcast(MESSAGE_LIST, all_nodes, generate_data(size_list))
    COUNT_LIST += 1

def change_state(new_state):
    global CURRENT_STATE
    global current_state_start_millis
    
    CURRENT_STATE = new_state
    current_state_start_millis = get_millis()

def generate_data(size):
    return '\x21' * size #! character

# sniff OLSR traffic to determine neighbor nodes
def sniff_neighbors():
    sniff_start_millis = get_millis()
    current_millis = sniff_start_millis
    sock_sniff = None

    try:
        sock_sniff = socket.socket( socket.AF_PACKET , socket.SOCK_RAW , socket.ntohs(0x0003))
    except socket.error, (value,message):
        if sock_sniff:
            sock_sniff.close()
    
        print 'ERROR, Could not open socket: ' + message
        sys.exit(1)
    
    while current_millis - sniff_start_millis < sniff_timeout:
        current_millis = get_millis()
    
        packet = sock_sniff.recvfrom(65565)
         
        #packet string from tuple
        packet = packet[0]
         
        #parse ethernet header
        eth_length = 14
         
        eth_header = packet[:eth_length]
        eth = struct.unpack('!6s6sH' , eth_header)
        eth_protocol = socket.ntohs(eth[2])
        
        #Parse IP packets, IP Protocol number = 8
        if eth_protocol == 8 :
            #Parse IP header
            #take first 20 characters for the ip header
            ip_header = packet[eth_length:20+eth_length]
             
            #now unpack them :)
            iph = struct.unpack('!BBHHHBBH4s4s' , ip_header)
     
            version_ihl = iph[0]
            ihl = version_ihl & 0xF
     
            iph_length = ihl * 4
    
            #ttl = iph[5]
            protocol = iph[6]
            src_ip = socket.inet_ntoa(iph[8]);
            #dst_ip = socket.inet_ntoa(iph[9]);
 
            if protocol == 17 :
                u = iph_length + eth_length
                #udph_length = 8
                udp_header = packet[u:u+8]
 
                #now unpack them :)
                udph = struct.unpack('!HHHH' , udp_header)
                source_port = udph[0]
                #dest_port = udph[1]

                #limit to OLSR packets
                if source_port == 698:
                    #print myid + ': OLSR packet from ' + str(src_ip)
                    update_neighbor(src_ip)
    sock_sniff.close()

def run_process(exe):
    p = subprocess.Popen(exe, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    while(True):
        retcode = p.poll() #returns None while subprocess is running
        line = p.stdout.readline()
        yield line
        if(retcode is not None):
            break

#ospf_neighbor_script = os.path.dirname(os.path.realpath(__file__)) + '/neighbors.sh'
def update_ospf_neighbors():
    ospf_neighbor_script = dir_name + '/neighbors.sh'
    for neighbor_ip in run_process(ospf_neighbor_script.split()):
        if '.' in neighbor_ip:
            update_neighbor(neighbor_ip.rstrip())

def open_sockets():
    global sock_local
    global sock_bcast
    global sockets
    
    try:
        # unicast
        sock_local = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock_local.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock_local.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock_local.bind(local_address)
        
        # broadcast
        sock_bcast = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock_bcast.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock_bcast.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock_bcast.bind(bcast_address)
        sock_bcast.setblocking(0)
        
        # combined
        sockets = [sock_local,sock_bcast]
    except socket.error, (value,message):
        if sock_local:
            sock_local.close()
        if sock_bcast:
            sock_bcast.close()
        print 'ERROR, Could not open socket(s): ' + message
        sys.exit(1)

def listen_sockets(timeout_function):
    while True:
        inputready,outputready,exceptready = select.select(sockets,[],[],select_timeout)
        # hit select_timeout and no sockets have messages
        if not (inputready or outputready or exceptready):
            if timeout_function() == 1:
                break
            else:
                continue
        # check for message on sockets
        for s in inputready:
            # unicast message
            if s == sock_local:
                data, addr = sock_local.recvfrom(buffer_size)
                #print data + ' from ' + src_ip
                #if (src_ip != local_ip):
                    #print myid + ':UNICAST FROM ' + src_ip + ': ' + data
            # broadcast message
            elif s == sock_bcast:
                data, addr = sock_bcast.recvfrom(buffer_size)
                #print data + ' from ' + src_ip
                #if (src_ip != local_ip):
                    #print myid + ':BROADCAST FROM ' + src_ip + ': ' + data
            else:
                print myid + ' ERROR: received on unknown socket'
                continue
            
            # process message
            src_ip = addr[0]
            update_neighbor(src_ip)
            check_neighbor_timeouts()
            if src_ip != local_ip:
                # the packet is a valid message
                if data.startswith(MESSAGE_COMMIT) or data.startswith(MESSAGE_DONE) or data.startswith(MESSAGE_LIST):
                    message = Message.fromString(data)
                    write_log(str(get_millis()) + ': received ' + message.getLogString() + ' from ' + str(src_ip) + '\n')
                    process_message(message, src_ip)
                    #print message

#update neighbor contact time
def update_neighbor(neighbor_ip):
    if neighbor_ip != local_ip:
        neighbor_nodes[neighbor_ip] = get_millis()
        if debug: 
            print myid + ': added/updated neighbor: ' + str(neighbor_ip)

def refresh_neighbors():
    #TODO clear neighbor_nodes and call update_ospf_neighbors
    print 'TODO'

#TODO: update via NHDP
def check_neighbor_timeouts():
    current_millis = get_millis()
    for neighbor in neighbor_nodes.keys():
        if current_millis - neighbor_nodes[neighbor] > neighbor_timeout:
            del neighbor_nodes[neighbor]
            if debug: 
                print myid + ': deleting neighbor due to timeout: ' + str(neighbor)

def main_timeout():
    global CURRENT_STATE
    global current_state_start_millis
    global done_retries
    global new_node
    global new_node_millis
    
    #node list update
    if new_node:
        current_millis = get_millis()
        if current_millis - new_node_millis > list_timeout:
            print myid + ': hit LIST timeout...'
            print myid + ': sending LIST broadcast'
            send_list_broadcast()
            new_node = False
    
    #sent COMMIT, waiting for DONE
    if CURRENT_STATE == STATE_WAIT_DONE:
        current_millis = get_millis()
        if current_millis - current_state_start_millis > done_timeout:
            done_retries -= 1
            if debug: 
                print myid + ': hit DONE timeout...'
                print myid + ': RETRIES = ' + str(done_retries)
            # no neighbors or all done or timeout
            if (len(list(set(neighbor_nodes) - set(executed_nodes))) == 0) or (done_retries == 0):
                if debug: 
                    if done_retries == 0:
                        print myid + ': out of retries, missing responses from ' + str(list(set(neighbor_nodes) - set(executed_nodes)))
                    else:
                        print myid + ': no neighbors or all executed'
                if root:
                    change_state(STATE_EXECUTE)
                    print myid + ': -> EXECUTE'
                else:
                    if confirmation:
                        if debug: 
                            print myid + ': broadcasting DONE'
                        send_done_broadcast()
                    change_state(STATE_EXECUTE)
                    print myid + ': -> EXECUTE'
            else:
                if debug: 
                    print myid + ': waiting for neighbors ' + str(list(set(neighbor_nodes) - set(executed_nodes)))
                    print myid + ': resending COMMIT broadcast'
                send_commit_broadcast()
            current_state_start_millis = get_millis()
    #execute configuration
    elif CURRENT_STATE == STATE_EXECUTE:
        current_millis = get_millis()
        if current_millis - current_state_start_millis > executed_timeout:
            if debug: 
                print myid + ': hit execute timeout...'
            execute()
    #abort configuration
    elif CURRENT_STATE == STATE_ABORT:
        abort()
    return 0

#process valid messages
def process_message(message, src_ip):
    global CURRENT_STATE
    global current_state_start_millis
    global start_millis
    global new_node
    global new_node_millis
 
    #received LIST message
    if message.getType() == MESSAGE_LIST:
        print myid + ': LIST from ' + src_ip
        if not (src_ip in all_nodes):
            all_nodes.append(src_ip)
            new_node = True
            if debug:
                print myid + ': new node ' + src_ip
            new_node_millis = get_millis()
        for node_ip in message.getNodeList():
            if not (node_ip in all_nodes):
                all_nodes.append(node_ip)
                new_node = True
                if debug:
                    print myid + ': new node ' + node_ip
                new_node_millis = get_millis()
    #received COMMIT message
    elif message.getType() == MESSAGE_COMMIT:
        print myid + ': COMMIT from ' + src_ip
        if not (src_ip in executed_nodes):
            if debug: 
                print myid + ': DONE from new node ' + src_ip
            executed_nodes.append(src_ip)
        for node_ip in message.getNodeList():
            if not (node_ip in executed_nodes):
                if debug: 
                    print myid + ': DONE from new node ' + node_ip
                executed_nodes.append(node_ip)
        if CURRENT_STATE == STATE_WAIT_COMMIT:
            if not root:
                start_millis = get_millis()
                send_commit_broadcast()
                if debug: 
                    print myid + ': sent COMMIT broadcast'
                if confirmation:
                    change_state(STATE_WAIT_DONE)
                    print myid + ': -> WAIT_DONE'
                else:
                    change_state(STATE_EXECUTE)
                    print myid + ': -> EXECUTE'
        else:
            if debug: 
                print myid + ': already sent COMMIT...'
    #received DONE message
    elif message.getType() == MESSAGE_DONE:
        print myid + ': DONE from ' + src_ip
        #add/extend list of executed nodes
        if not (src_ip in executed_nodes):
            if debug: 
                print myid + ': DONE from new node ' + src_ip
            executed_nodes.append(src_ip)
        for node_ip in message.getNodeList():
            if not (node_ip in executed_nodes):
                if debug: 
                    print myid + ': DONE from new node ' + node_ip
                executed_nodes.append(node_ip)
    else:
        print myid + ': ERROR unknown message type ' + message.getType() + ' from ' + src_ip

def execute():
    global log
    
    stop_millis = get_millis()
    print myid + '(' + local_ip + ') executed in ' + str(stop_millis - start_millis) + ' msec'
    
    if debug: 
        print myid + ' all node list: ' + str(sorted(all_nodes))
        print myid + ' neighbor list: ' + str(sorted(neighbor_nodes))
        print myid + ' executed list: ' + str(sorted(executed_nodes))
        print myid + ' counts: COMMIT=' + str(COUNT_COMMIT) + ', DONE=' + str(COUNT_DONE) + ', LIST=' + str(COUNT_LIST)
    
    write_log(str(get_millis()) + ': rollout executed\n')
    log.close()
    sock_local.close()
    #sock_bcast.close()
    log = open(exec_file, 'w')
    log.write(myid)
    log.close()
    exit(0)

def abort():
    global log
    
    print myid + '(' + local_ip + ') aborted'
    
    if debug: 
        print myid + ' all node list: ' + str(sorted(all_nodes))
        print myid + ' neighbor list: ' + str(sorted(neighbor_nodes))
    
    write_log(str(get_millis()) + ': rollout aborted\n')
    log.close()
    sock_local.close()
    log = open(abort_file, 'w')
    log.write(myid)
    log.close()
    exit(1)

# MAIN ENTRY POINT
if (len(sys.argv) < 2):
    print("Usage: conf_unconditional.py <node ID (0 for root)> ?<config file>")
    exit(0)

dir_name = os.path.dirname(os.path.abspath(__file__))

# set parameters
myid = sys.argv[1]

#load configuration file
if len(sys.argv) > 2:
    if os.path.isfile(sys.argv[2]):
        config_file = sys.argv[2]
    else:
        print 'ERROR: invalid config path, ' + sys.argv[2]
        sys.exit()
config = ConfigParser.RawConfigParser()
config.read(config_file)

root = False
if str(myid) == '0':
    root = True
iface = config.get('General', 'iface')#'eth0'#emane0#lo
local_ip = get_ip_address(iface)
local_ip_test = "'" + local_ip + "'"
bcast_ip = get_broadcast_address(iface)
port = config.getint('General', 'port')#2000
local_address = (local_ip, port)
bcast_address = (bcast_ip, port)
buffer_size = 1024

log_dir = dir_name + '/logs/'
if not os.path.exists(log_dir):
    os.mkdir(log_dir)
log_file = log_dir + 'node_' + myid + '.log'
exec_file = log_dir + 'exec_' + myid + '.log'
abort_file = log_dir + 'abort_' + myid + '.log'
log = open(log_file, 'w')

#general
conditional = config.getboolean('General', 'conditional')
if conditional:
    print 'WARNING: trying to use conditional configuration for unconditional script'
    #sys.exit()
confirmation = config.getboolean('General', 'confirmation')
debug = config.getboolean('General', 'debug')
neighbor_mode = config.getint('General', 'neighbor_mode')#0 = sniff OLSR (EMANE), 1 = query OSPF (CORE)

#timeouts
sniff_timeout = config.getint('Timeout', 't_sniff')#1000
pending_timeout = config.getint('Timeout', 't_pending')#1000#500
ack_timeout = config.getint('Timeout', 't_ack')#pending_timeout
done_timeout = config.getint('Timeout', 't_done')#pending_timeout
decision_timeout = config.getint('Timeout', 't_decision')#pending_timeout
commit_timeout = config.getint('Timeout', 't_commit')#20000
neighbor_timeout = config.getint('Timeout', 't_neighbor')#40000
select_timeout = config.getint('Timeout', 't_select')#0#1
executed_timeout = config.getint('Timeout', 't_executed')#1000#500
list_timeout = config.getint('Timeout', 't_list')#5000#2000

#retries
#retries = 10
done_retries = config.getint('Retry', 'r_done')#retries
ack_retries = config.getint('Retry', 'r_ack')#retries

#sizes
size_confirm = config.getint('Size', 's_confirm')##SIZE_CONFIRM = 10000
size_ack = config.getint('Size', 's_ack')#SIZE_ACK = 50
size_commit = config.getint('Size', 's_commit')#SIZE_COMMIT = 20000
size_done = config.getint('Size', 's_done')#SIZE_DONE = 10
size_list = config.getint('Size', 's_list')#SIZE_LIST = 0

if debug: 
    print 'unconditional rollout with' + ('' if confirmation else 'out') + ' confirmation (' + version_id + ')'
    print 'using configuration: ' + config_file
    print '=============================================='

if debug: 
    print 'ID: ' + myid + ('(ROOT)' if root else '')
    print 'Interface: ' + iface
    print 'Local IP: ' + local_ip
    print 'Bcast IP: ' + bcast_ip
    print 'Port: ' + str(port)
    print 'Retries: ack ' + str(ack_retries) + ', done ' + str(done_retries)
    print 'Timeouts: sniff ' + str(sniff_timeout) + ', neighbor ' + str(neighbor_timeout) + ', pending/ack/done/decision ' + str(pending_timeout) + ', commit ' + str(commit_timeout) + ', executed ' + str(executed_timeout)
    print 'Sizes: confirm ' + str(size_confirm) + ', ack ' + str(size_ack) + ', commit ' + str(size_commit) + ', done ' + str(size_done) + ', list ' + str(size_list)

if root:
    write_log('log: node=' + myid + ' ip=' + str(local_ip) + ' (ROOT NODE)\n')
else:
    write_log('log: node=' + myid + ' ip=' + str(local_ip) + '\n')

#sockets
sock_local = None
sock_bcast = None
sockets = []

#node lists
neighbor_nodes = {}
all_nodes = []
executed_nodes = []
new_node = False
new_node_millis = get_millis()
#exit_status = 0

# sniff for neighbors
CURRENT_STATE = STATE_SNIFF
current_state_start_millis = get_millis()
if confirmation:
    if neighbor_mode == 0:
        if debug: 
            print '===================================='
            print 'sniffing OLSR packets to build neighbor list...'
        sniff_neighbors()
    elif neighbor_mode == 1:
        if debug:
            print '===================================='
            print 'querying OSPF neighbors...'
        update_ospf_neighbors()
    else:
        print 'ERROR: unknown neighbor mode ' + str(neighbor_mode)
        sys.exit()
    print myid + ': 1-hop neighbors: ' + str(neighbor_nodes.keys())
    for neighbor in neighbor_nodes.keys():
        all_nodes.append(neighbor)
        
# setup sockets
open_sockets()

if confirmation:
    if debug:
        print 'sending node list'
    send_list_broadcast()

# main loop
if debug: 
    print '===================================='
    print 'entering main loop...'

#root behavior
if root:
    print myid + '(ROOT) waiting 3 seconds...'
    time.sleep(3)
    #manual_delay = raw_input('press enter to send')
    print myid + '(ROOT) sending initial COMMIT'
    send_commit_broadcast()
#    for neighbor in neighbor_nodes:
#        send_commit_unicast(neighbor)
    start_millis = get_millis()
    if confirmation:
        change_state(STATE_WAIT_DONE)
        print myid + ': -> WAIT_DONE'
    else:
        change_state(STATE_EXECUTE)
        print myid + ': -> EXECUTE'
else:
    change_state(STATE_WAIT_COMMIT)
    print myid + ': -> WAIT_COMMIT'
    
listen_sockets(main_timeout)
