#!/usr/bin/env python
"""
Generates configuration files
- custom if path passed as argument
- default if no arguments

Created on Tue Nov 11 11:07:22 2014
Updated 1/27/2015
@author: Prateek Singh
"""

import ConfigParser
import os
import sys

dir_name = os.path.dirname(os.path.abspath(__file__))

config_folder = dir_name + '/config/'

core_folder = config_folder + 'core/'
emane_folder = config_folder + 'emane/'

core_a_folder = core_folder + 'a/'
core_b_folder = core_folder + 'b/'
core_c_folder = core_folder + 'c/'
core_d_folder = core_folder + 'd/'

emane_a_folder = emane_folder + 'a/'
emane_b_folder = emane_folder + 'b/'
emane_c_folder = emane_folder + 'c/'
emane_d_folder = emane_folder + 'd/'

config_emane_a_none = emane_a_folder + 'none.cfg'
config_emane_a_small = emane_a_folder + 'small.cfg'
config_emane_a_medium = emane_a_folder + 'medium.cfg'
config_emane_a_large = emane_a_folder + 'large.cfg'
config_emane_b_none = emane_b_folder + 'none.cfg'
config_emane_b_small = emane_b_folder + 'small.cfg'
config_emane_b_medium = emane_b_folder + 'medium.cfg'
config_emane_b_large = emane_b_folder + 'large.cfg'
config_emane_c_none = emane_c_folder + 'none.cfg'
config_emane_c_small = emane_c_folder + 'small.cfg'
config_emane_c_medium = emane_c_folder + 'medium.cfg'
config_emane_c_large = emane_c_folder + 'large.cfg'
config_emane_d_none = emane_d_folder + 'none.cfg'
config_emane_d_small = emane_d_folder + 'small.cfg'
config_emane_d_medium = emane_d_folder + 'medium.cfg'
config_emane_d_large = emane_d_folder + 'large.cfg'

config_core_a_none = core_a_folder + 'none.cfg'
config_core_a_small = core_a_folder + 'small.cfg'
config_core_a_medium = core_a_folder + 'medium.cfg'
config_core_a_large = core_a_folder + 'large.cfg'
config_core_b_none = core_b_folder + 'none.cfg'
config_core_b_small = core_b_folder + 'small.cfg'
config_core_b_medium = core_b_folder + 'medium.cfg'
config_core_b_large = core_b_folder + 'large.cfg'
config_core_c_none = core_c_folder + 'none.cfg'
config_core_c_small = core_c_folder + 'small.cfg'
config_core_c_medium = core_c_folder + 'medium.cfg'
config_core_c_large = core_c_folder + 'large.cfg'
config_core_d_none = core_d_folder + 'none.cfg'
config_core_d_small = core_d_folder + 'small.cfg'
config_core_d_medium = core_d_folder + 'medium.cfg'
config_core_d_large = core_d_folder + 'large.cfg'

try:
	if not os.path.exists(config_folder):
		os.mkdir(config_folder)
except OSError:
	print 'ERROR: could not create output folder: ' + config_folder
	pass

def write_config(config_file, 
                 iface, port, conditional, confirmation, debug, neighbor_mode,
                 sniff_timeout, pending_timeout, ack_timeout, done_timeout, decision_timeout, commit_timeout, neighbor_timeout, select_timeout, executed_timeout, list_timeout,
                 done_retries, ack_retries,
                 size_confirm, size_ack, size_commit, size_done, size_list):
    config = ConfigParser.RawConfigParser()

    #general
    config.add_section('General')
    config.set('General', 'iface', str(iface))
    config.set('General', 'port', str(port))
    config.set('General', 'conditional', str(conditional).lower())
    config.set('General', 'confirmation', str(confirmation).lower())
    config.set('General', 'debug', str(debug).lower())
    config.set('General', 'neighbor_mode', str(neighbor_mode))
    #timeouts
    config.add_section('Timeout')
    config.set('Timeout', 't_sniff', str(sniff_timeout))
    config.set('Timeout', 't_pending', str(pending_timeout))
    config.set('Timeout', 't_ack', str(ack_timeout))
    config.set('Timeout', 't_done', str(done_timeout))
    config.set('Timeout', 't_decision', str(decision_timeout))
    config.set('Timeout', 't_commit', str(commit_timeout))
    config.set('Timeout', 't_neighbor', str(neighbor_timeout))
    config.set('Timeout', 't_select', str(select_timeout))
    config.set('Timeout', 't_executed', str(executed_timeout))
    config.set('Timeout', 't_list', str(list_timeout))
    #retries
    config.add_section('Retry')
    config.set('Retry', 'r_done', str(done_retries))
    config.set('Retry', 'r_ack', str(ack_retries))
    #sizes
    config.add_section('Size')
    config.set('Size', 's_confirm', str(size_confirm))
    config.set('Size', 's_ack', str(size_ack))
    config.set('Size', 's_commit', str(size_commit))
    config.set('Size', 's_done', str(size_done))
    config.set('Size', 's_list', str(size_list))
    
    with open(config_file, 'wb') as configfile:
        config.write(configfile)
    print 'wrote configuration file: ' + config_file

def load_config(config_file):
    config = ConfigParser.RawConfigParser()
    config.read(config_file)
    
    print 'Config: ' + config_file
    print '--------' + '-'*len(config_file)
    #general
    print 'General'
    iface = config.get('General', 'iface')
    print 'interface: ' + str(iface)
    port = config.getint('General', 'port')
    print 'port: ' + str(port)
    conditional = config.getboolean('General', 'conditional')
    print 'conditional: ' + str(conditional)
    confirmation = config.getboolean('General', 'confirmation')
    print 'confirmation: ' + str(confirmation)
    debug = config.getboolean('General', 'debug')
    print 'debug: ' + str(debug)
    neighbor_mode = config.getint('General', 'neighbor_mode')
    print 'neighbor mode: ' + str(neighbor_mode) + '\n'
    #timeouts
    print 'Timeouts'
    sniff_timeout = config.getint('Timeout', 't_sniff')
    print 'sniff: ' + str(sniff_timeout)
    pending_timeout = config.getint('Timeout', 't_pending')
    print 'pending: ' + str(pending_timeout)
    ack_timeout = config.getint('Timeout', 't_ack')
    print 'ack: ' + str(ack_timeout)
    done_timeout = config.getint('Timeout', 't_done')
    print 'done: ' + str(done_timeout)
    decision_timeout = config.getint('Timeout', 't_decision')
    print 'decision: ' + str(decision_timeout)
    commit_timeout = config.getint('Timeout', 't_commit')
    print 'commit: ' + str(commit_timeout)
    neighbor_timeout = config.getint('Timeout', 't_neighbor')
    print 'neighbor: ' + str(neighbor_timeout)
    select_timeout = config.getint('Timeout', 't_select')
    print 'select: ' + str(select_timeout)
    executed_timeout = config.getint('Timeout', 't_executed')
    print 'executed: ' + str(executed_timeout)
    list_timeout = config.getint('Timeout', 't_list')
    print 'list: ' + str(list_timeout) + '\n'
    #retries
    print 'Retries'
    done_retries = config.getint('Retry', 'r_done')
    print 'done: ' + str(done_retries)
    ack_retries = config.getint('Retry', 'r_ack')
    print 'ack: ' + str(ack_retries) + '\n'
    #sizes
    print 'Sizes'
    size_confirm = config.getint('Size', 's_confirm')
    print 'confirm: ' + str(size_confirm)
    size_ack = config.getint('Size', 's_ack')
    print 'ack: ' + str(size_ack)
    size_commit = config.getint('Size', 's_commit')
    print 'commit: ' + str(size_commit)
    size_done = config.getint('Size', 's_done')
    print 'done: ' + str(size_done)
    size_list = config.getint('Size', 's_list')
    print 'list: ' + str(size_list) + '\n'

def custom_config(config_file):
    #config_file = raw_input('config file name: ')
    config_custom = config_folder + config_file + '.cfg'
    iface= raw_input('interface: ')
    port= raw_input('port: ')
    conditional= raw_input('conditional? [False, True]: ')
    confirmation= raw_input('confirmation? [False, True]: ')
    debug= raw_input('debug? [False, True]: ')
    neighbor_mode= raw_input('neighbor mode? [0, 1]: ')
    sniff_timeout= raw_input('sniff timeout: ')
    pending_timeout= raw_input('pending timeout: ')
    ack_timeout= raw_input('ACK timeout: ')
    done_timeout= raw_input('DONE timeout: ')
    decision_timeout= raw_input('decision timeout: ')
    commit_timeout= raw_input('COMMIT timeout: ')
    neighbor_timeout= raw_input('neighbor timeout: ')
    select_timeout= raw_input('select timeout (0): ')
    executed_timeout= raw_input('executed timeout: ')
    list_timeout= raw_input('LIST timeout: ')
    done_retries= raw_input('DONE retries: ')
    ack_retries= raw_input('ACK retries: ')
    size_confirm= raw_input('CONFIRM size (bytes): ')
    size_ack= raw_input('ACK size (bytes): ')
    size_commit= raw_input('COMMIT size (bytes): ')
    size_done= raw_input('DONE size (bytes): ')
    size_list= raw_input('LIST size (bytes): ')
    write_config(config_custom, iface, port, conditional, confirmation, debug, neighbor_mode, sniff_timeout, pending_timeout, ack_timeout, done_timeout, decision_timeout, commit_timeout, neighbor_timeout, select_timeout, executed_timeout, list_timeout, done_retries, ack_retries, size_confirm, size_ack, size_commit, size_done, size_list)
    load_config(config_custom)

def write_default_configs():
    '''
    config_file, 
    iface, port, conditional, confirmation, debug, neighbor_mode
    sniff_timeout, pending_timeout, ack_timeout, done_timeout, decision_timeout, commit_timeout, neighbor_timeout, select_timeout, executed_timeout, list_timeout,
    done_retries, ack_retries,
    size_confirm, size_ack, size_commit, size_done, size_list
    '''

    print 'generating default configuration files...'

    #emane
    print 'EMANE'
    print 'mode A'
    write_config(config_emane_a_none, 'emane0', 2000, False, False, True, 0, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 0, 0, 0, 0, 0)
    write_config(config_emane_a_small, 'emane0', 2000, False, False, False, 0, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 1000, 100, 1000, 100, 0)
    write_config(config_emane_a_medium, 'emane0', 2000, False, False, False, 0, 1000, 750, 750, 750, 1000, 10000, 20000, 0, 1000, 3000, 10, 10, 5000, 1000, 5000, 1000, 0)
    write_config(config_emane_a_large, 'emane0', 2000, False, False, False, 0, 1000, 1000, 1000, 1000, 1000, 20000, 40000, 0, 1000, 5000, 10, 10, 10000, 5000, 10000, 5000, 0)
    print 'mode B'
    write_config(config_emane_b_none, 'emane0', 2000, False, True, True, 0, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 0, 0, 0, 0, 0)
    write_config(config_emane_b_small, 'emane0', 2000, False, True, False, 0, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 1000, 100, 1000, 100, 0)
    write_config(config_emane_b_medium, 'emane0', 2000, False, True, False, 0, 1000, 750, 750, 750, 1000, 10000, 20000, 0, 1000, 3000, 10, 10, 5000, 1000, 5000, 1000, 0)
    write_config(config_emane_b_large, 'emane0', 2000, False, True, False, 0, 1000, 1000, 1000, 1000, 1000, 20000, 40000, 0, 1000, 5000, 10, 10, 10000, 5000, 10000, 5000, 0)
    print 'mode C'
    write_config(config_emane_c_none, 'emane0', 2000, True, False, True, 0, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 0, 0, 0, 0, 0)
    write_config(config_emane_c_small, 'emane0', 2000, True, False, False, 0, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 1000, 100, 1000, 100, 0)
    write_config(config_emane_c_medium, 'emane0', 2000, True, False, False, 0, 1000, 750, 750, 750, 1000, 10000, 20000, 0, 1000, 3000, 10, 10, 5000, 1000, 5000, 1000, 0)
    write_config(config_emane_c_large, 'emane0', 2000, True, False, False, 0, 1000, 1000, 1000, 1000, 1000, 20000, 40000, 0, 1000, 5000, 10, 10, 10000, 5000, 10000, 5000, 0)
    print 'mode D'
    write_config(config_emane_d_none, 'emane0', 2000, True, True, True, 0, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 0, 0, 0, 0, 0)
    write_config(config_emane_d_small, 'emane0', 2000, True, True, False, 0, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 1000, 100, 1000, 100, 0)
    write_config(config_emane_d_medium, 'emane0', 2000, True, True, False, 0, 1000, 750, 750, 750, 1000, 10000, 20000, 0, 1000, 3000, 10, 10, 5000, 1000, 5000, 1000, 0)
    write_config(config_emane_d_large, 'emane0', 2000, True, True, False, 0, 1000, 1000, 1000, 1000, 1000, 20000, 40000, 0, 1000, 5000, 10, 10, 10000, 5000, 10000, 5000, 0)
    #core
    print 'CORE'
    print 'mode A'
    write_config(config_core_a_none, 'eth0', 2000, False, False, True, 1, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 0, 0, 0, 0, 0)
    write_config(config_core_a_small, 'eth0', 2000, False, False, False, 1, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 1000, 100, 1000, 100, 0)
    write_config(config_core_a_medium, 'eth0', 2000, False, False, False, 1, 1000, 750, 750, 750, 1000, 10000, 20000, 0, 1000, 3000, 10, 10, 5000, 1000, 5000, 1000, 0)
    write_config(config_core_a_large, 'eth0', 2000, False, False, False, 1, 1000, 1000, 1000, 1000, 1000, 20000, 40000, 0, 1000, 5000, 10, 10, 10000, 5000, 10000, 5000, 0)
    print 'mode B'
    write_config(config_core_b_none, 'eth0', 2000, False, True, True, 1, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 0, 0, 0, 0, 0)
    write_config(config_core_b_small, 'eth0', 2000, False, True, False, 1, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 1000, 100, 1000, 100, 0)
    write_config(config_core_b_medium, 'eth0', 2000, False, True, False, 1, 1000, 750, 750, 750, 1000, 10000, 20000, 0, 1000, 3000, 10, 10, 5000, 1000, 5000, 1000, 0)
    write_config(config_core_b_large, 'eth0', 2000, False, True, False, 1, 1000, 1000, 1000, 1000, 1000, 20000, 40000, 0, 1000, 5000, 10, 10, 10000, 5000, 10000, 5000, 0)
    print 'mode C'
    write_config(config_core_c_none, 'eth0', 2000, True, False, True, 1, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 0, 0, 0, 0, 0)
    write_config(config_core_c_small, 'eth0', 2000, True, False, False, 1, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 1000, 100, 1000, 100, 0)
    write_config(config_core_c_medium, 'eth0', 2000, True, False, False, 1, 1000, 750, 750, 750, 1000, 10000, 20000, 0, 1000, 3000, 10, 10, 5000, 1000, 5000, 1000, 0)
    write_config(config_core_c_large, 'eth0', 2000, True, False, False, 1, 1000, 1000, 1000, 1000, 1000, 20000, 40000, 0, 1000, 5000, 10, 10, 10000, 5000, 10000, 5000, 0)
    print 'mode D'
    write_config(config_core_d_none, 'eth0', 2000, True, True, True, 1, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 0, 0, 0, 0, 0)
    write_config(config_core_d_small, 'eth0', 2000, True, True, False, 1, 1000, 500, 500, 500, 1000, 2000, 10000, 0, 1000, 2000, 10, 10, 1000, 100, 1000, 100, 0)
    write_config(config_core_d_medium, 'eth0', 2000, True, True, False, 1, 1000, 750, 750, 750, 1000, 10000, 20000, 0, 1000, 3000, 10, 10, 5000, 1000, 5000, 1000, 0)
    write_config(config_core_d_large, 'eth0', 2000, True, True, False, 1, 1000, 1000, 1000, 1000, 1000, 20000, 40000, 0, 1000, 5000, 10, 10, 10000, 5000, 10000, 5000, 0)
    
# MAIN ENTRY POINT
if (len(sys.argv) > 1):
    custom_config(sys.argv[1])
else:
    write_default_configs()
