#!/bin/bash

if [ -z "$1" ]; then
	echo usage: $0 'nodes config[{a,b,c,d}/{none,small,medium,large}]'
	exit
else
	NODES=$1
fi

if [ -z "$2" ]; then
	echo usage: $0 'nodes config[{a,b,c,d}/{none,small,medium,large}]'
	exit
else
	CONFIGNAME="emane/$2"
fi

MODE="$(echo $CONFIGNAME | cut -d'/' -f 2)"

if [ $MODE = "a" ]; then
	CONDITION="unconditional"
	CONFIRMATION="false"
elif [ $MODE = "b" ]; then
	CONDITION="unconditional"
	CONFIRMATION="true"
elif [ $MODE = "c" ]; then
	CONDITION="conditional"
	CONFIRMATION="false"
elif [ $MODE = "d" ]; then
	CONDITION="conditional"
	CONFIRMATION="true"
else
	echo "ERROR: unknown mode: $MODE"
	exit
fi

CONFIG="CONF/config/$CONFIGNAME.cfg"
PLATFORM="$(echo $CONFIGNAME | cut -d'/' -f 1)"
CFG="$(echo $CONFIGNAME | cut -d'/' -f 3)"
TYPE="$(echo $CONDITION | head -c 1)"
CONF="$(echo $CONFIRMATION | head -c 1)"
SIZE="$(echo $CONFIGNAME | cut -d'/' -f 3 | head -c 1)"

DIVISOR=3
DELAY=5

echo "SINGLE RUN"
echo "Platform: $PLATFORM"
echo "Nodes: $NODES"
echo "Mode: $MODE"
echo "Condition: $CONDITION"
echo "Confirmation: $CONFIRMATION"
echo "Configuration: $CFG"

REGULAR=$(($NODES - 1))
echo "Loading $REGULAR agents:"

#start agents and allow a bit of time
for ID in $(seq 1 $REGULAR); do 
	#ssh -n -t -t node-$ID "sudo ./CONF/conf_$CONDITION.py $ID $CONFIG" > /dev/null 2>&1 &
	ssh -n -t -t node-$ID "sudo ./CONF/conf.py $ID $CONFIG" > /dev/null 2>&1 &
done

#wait for nodes to start
sleep $DELAY

#ps ax | grep '[c]onf.py' | wc -l
#echo "press any key to start root node..."
#read input

#check running nodes
NODECOUNT=$(ps ax | grep '[c]onf.py' | wc -l)
NODECOUNT=$(($NODECOUNT / $DIVISOR))
echo "$NODECOUNT running nodes"
COUNTER=10
until [ $NODECOUNT -ge $REGULAR ]; do
	echo "WARNING: MISSING NODES, $NODECOUNT running, $COUNTER wait(s) remaining"
	if [ $COUNTER -le 0 ]; then
		read -p "hit ENTER to continue"
	fi
	sleep 2
	NODECOUNT=$(ps ax | grep '[c]onf.py' | wc -l)
	NODECOUNT=$(($NODECOUNT / $DIVISOR))
	echo "$NODECOUNT running nodes"
	let COUNTER-=1
done
#if [[ $NODECOUNT -lt $REGULAR ]]; then
#	echo "WARNING: MISSING NODES"
#	read -p "hit ENTER to continue"
#fi

echo "Starting ROOT:"
#ssh -t -t node-$NODES "sudo ./CONF/conf_$CONDITION.py 0 $CONFIG" > /dev/null 2>&1
ssh -t -t node-$NODES "sudo ./CONF/conf.py 0 $CONFIG" > /dev/null 2>&1

#allow more time to account for any applied delay
sleep $DELAY
#check running nodes
NODECOUNT=$(ps ax | grep '[c]onf.py' | wc -l)
NODECOUNT=$(($NODECOUNT / $DIVISOR))
echo "$NODECOUNT running nodes"
COUNTER=10
until [ $NODECOUNT -eq 0 ]; do
	echo "WARNING: $NODECOUNT running nodes, $COUNTER wait(s) remaining"
	if [ $COUNTER -le 0 ]; then
		echo "killing nodes..."
		sudo kill $(ps aux | grep '[c]onf.py' | awk '{print $2}')
	fi
	sleep 2
	NODECOUNT=$(ps ax | grep '[c]onf.py' | wc -l)
	NODECOUNT=$(($NODECOUNT / $DIVISOR))
	echo "$NODECOUNT running nodes"
	let COUNTER-=1
done
#if [[ $NODECOUNT -gt 0 ]]; then
#	echo "WARNING: $NODECOUNT NODES STILL RUNNING"
#	read -p "hit ENTER to kill"
#	sudo kill $(ps aux | grep '[c]onf.py' | awk '{print $2}')
#fi

#parse logs
python analysis.py ./ 0 $REGULAR

#remove logs
#rm -f logs/*

#move logs
mv logs/* analysis/

#move results
DATE=$(date +%Y%m%d%H%M%S)
OUTPUT="analysis_emane_$NODES"
OUTPUT+="_"
OUTPUT+=$MODE
OUTPUT+="_"
OUTPUT+=$TYPE
OUTPUT+="_"
OUTPUT+=$CONF
OUTPUT+="_"
OUTPUT+=$SIZE
OUTPUT+="_"
OUTPUT+=$DATE
mv analysis $OUTPUT
mv $OUTPUT results/

echo "Finished"
