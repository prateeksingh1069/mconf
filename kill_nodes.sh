#!/bin/bash

echo "checking for running nodes..."

#read -p "hit ENTER to continue"
#sudo kill $(ps aux | grep '[c]onf.py' | awk '{print $2}')

DIVISOR=2

#check running nodes
#change '[c]onf.py' to '[c]onf_' if using separate code
NODECOUNT=$(ps ax | grep '[c]onf.py' | wc -l)
NODECOUNT=$(($NODECOUNT / $DIVISOR))
echo "found $NODECOUNT running nodes"
if [[ $NODECOUNT -gt 0 ]]; then
	echo "killing nodes..."
	sudo kill $(ps aux | grep '[c]onf.py' | awk '{print $2}')
	echo "killed"
	NODECOUNT=$(ps ax | grep '[c]onf.py' | wc -l)
	NODECOUNT=$(($NODECOUNT / $DIVISOR))
	echo "found $NODECOUNT running nodes"
fi

echo "finished"
