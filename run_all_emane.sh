#!/bin/bash

#function set_topology {
#	echo "setting topology..."
#	./topology/pathloss_topology.py zconf.txt
#}

function set_loss {
	echo "setting loss to $1%..."
	./topology/commeffect_loss.py $1
	sleep 60
}

#read iteration count from command line
if [ -z "$1" ]; then
	echo usage: $0 'iterations'
	exit
else
	ITERATIONS=$1
fi

CLEANUP=0

echo "RUNNING ALL TESTS"
echo "Iterations: $ITERATIONS"

echo "NOTE: assuming that demo is already running!"
read -t 10 -p "hit ENTER or wait 10 seconds to start"; echo;

OLSRVIEW=$(ps ax | grep olsrlinkview.py | wc -l)
if [[ $OLSRVIEW -le 1 ]]; then
	echo "starting olsr link viewer..."
	./demo/scripts/olsrlinkview.py &
fi

#set_topology

MODES=("a" "b" "c" "d")
CONFIGS=("small" "medium" "large")
LOSSES=(0 5 10 15 20 25)
for MODE in ${MODES[@]}; do
	if [ $MODE = "a" ]; then
		CONDITION="unconditional"
		CONFIRMATION="false"
	elif [ $MODE = "b" ]; then
		CONDITION="unconditional"
		CONFIRMATION="true"
	elif [ $MODE = "c" ]; then
		CONDITION="conditional"
		CONFIRMATION="false"
	elif [ $MODE = "d" ]; then
		CONDITION="conditional"
		CONFIRMATION="true"
	else
		echo "ERROR: unknown mode: $MODE"
		continue
	fi
	for CONFIG in ${CONFIGS[@]}; do
		CONFIGNAME="$MODE/$CONFIG"
		for LOSS in ${LOSSES[@]}; do
			echo -e '\E[37;44m'"\033[1mmode $MODE, $CONDITION, $CONFIRMATION, $CONFIGNAME, $LOSS% loss\033[0m"
			set_loss $LOSS
			./run_batch_emane.sh $ITERATIONS $LOSS $CONFIGNAME
		done
	done
done

#process results
echo "processing results..."
DATE=$(date +%Y%m%d%H%M%S)

OUTPUT="analysis_all_emane"
OUTPUT+="_"
OUTPUT+=$DATE

if [ "$ITERATIONS" -eq 1 ]; then
	mv results/results_* $OUTPUT
	if [ "$CLEANUP" -eq 1 ]; then
		echo "Cleanup"
		rm -r -f $OUTPUT/results_*/_*
	fi
else
	mkdir $OUTPUT
	mv results/results_* $OUTPUT/

	#get overall durations,total packets
	cat $OUTPUT/results_*/durations_raw.csv > durations_all.csv
	cat $OUTPUT/results_*/durations_node_raw.csv > durations_node_all.csv
	cat $OUTPUT/results_*/totalpackets_raw.csv > totalpackets_all.csv
	cat $OUTPUT/results_*/totalpackets_node_raw.csv > totalpackets_node_all.csv
	cat $OUTPUT/results_*/execcount_raw.csv > execcount_all.csv
	echo '"loss","mode","condition","conf","size","duration"' | cat - durations_all.csv > durations.csv
	echo '"loss","mode","condition","conf","size","duration"' | cat - durations_node_all.csv > durations_node.csv
	echo '"loss","mode","condition","conf","size","packets"' | cat - totalpackets_all.csv > totalpackets.csv
	echo '"loss","mode","condition","conf","size","packets"' | cat - totalpackets_node_all.csv > totalpackets_node.csv
	echo '"loss","mode","condition","conf","size","executed"' | cat - execcount_all.csv > execcount.csv	

	#create plots
	R --file=plot_all.R > /dev/null 2>&1
	mv packets* $OUTPUT/
	mv durations* $OUTPUT/
	mv execpcts* $OUTPUT/
	mv totalpackets* $OUTPUT/
	mv execcount* $OUTPUT/
	if [ "$CLEANUP" -eq 1 ]; then
		echo "Cleanup"
		rm -r -f $OUTPUT/results_*/_*
	fi
fi

#move output folder
mv $OUTPUT results/

echo "Finished"
