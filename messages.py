#!/usr/bin/env python
"""
Configuration Message

Prateek Singh
"""

class Message(object):
	def __init__(self, message_type, message_id, src_ip, dst_ip, timestamp, node_list, data):
		self.message_type = message_type
		self.message_id = message_id
		self.timestamp = timestamp
		self.src_ip = src_ip
		self.dst_ip = dst_ip
		self.node_list = node_list
		self.data = data

	def getType(self):
		return self.message_type

	def getId(self):
		return self.message_id

	def getSrcIP(self):
		return self.src_ip

	def getDstIP(self):
		return self.dst_ip

	def getNodeList(self):
		return self.node_list

	def getData(self):
		return self.data

	def getLogString(self):
		message_string = self.message_type
		message_string += '|'
		message_string += str(self.message_id)
		message_string += '|'
		message_string += self.src_ip
		message_string += '|'
		message_string += self.dst_ip
		message_string += '|'
		message_string += str(self.timestamp)
		message_string += '|'
		#message_string += self.data
		message_string += '[' + str(len(self.data)) + ']'
		message_string += '|'
		message_string += str(self.node_list)
		return message_string

	def __str__(self):
		message_string = self.message_type
		message_string += '|'
		message_string += str(self.message_id)
		message_string += '|'
		message_string += self.src_ip
		message_string += '|'
		message_string += self.dst_ip
		message_string += '|'
		message_string += str(self.timestamp)
		message_string += '|'
		message_string += str(self.node_list)
		message_string += '|'
		message_string += str(self.data)
		#message_string += '[' + str(len(self.data)) + ']'
		return message_string

	@classmethod
	def fromString(cls, packet_data):
		parts = packet_data.split('|')
		message_type = parts[0]
		message_id = parts[1]
		src_ip = parts[2]
		dst_ip = parts[3]
		timestamp = parts[4]
		node_list = eval(parts[5])
		#data_size = parts[6]
		#data_size = int(data_size[1:len(data_size)-1])
		#data = '\x00' * data_size
		data = parts[6]
		return cls(message_type, message_id, src_ip, dst_ip, timestamp, node_list, data)

