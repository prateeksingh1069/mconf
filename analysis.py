#!/usr/bin/env python

import os
import re
import sys

version_id = '2015-01-25'
debug = False

if (len(sys.argv) != 4):
	print("Usage: analysis.py <path_to_logs> <minimum_node_ID> <maximum_node_ID>")
	print("parses logs named 'node_[n].log', where [n] across all logs represents")
	print("an unbroken range of integers, i.e. 0..9 or 3..6")
	exit(0)

if debug:
    print 'log analysis (' + version_id + ')'
    print '=============================================='

print 'processing logs...'

# set up variables
log_prefix = 'node_'
abort_prefix = 'abort_'
exec_prefix = 'exec_'

local_path = sys.argv[1]# + '/'
log_path = local_path + 'logs/'
output_path = local_path + 'analysis/'
min_log = int(sys.argv[2])
max_log = int(sys.argv[3])

log_filenames = []
files = []
lines = []

#TODO: count each message type
type_counts = {}
type_counts['CONFIRM'] = 0
type_counts['ACK'] = 0
type_counts['COMMIT'] = 0
type_counts['DONE'] = 0
type_counts['LIST'] = 0

abort_times = []
exec_times = []

exec_nodes = []
abort_nodes = []
exec_count = 0
abort_count = 0

node_sent_packets = []

highest_duration = 0
lowest_duration = 0
current_duration = 0
total_messages = 0

############################

re_timestamp = '(?P<tstamp>(\\d+))'
re_separator = '.*?'
re_action = '(?P<action>(sent|received))'
re_type = '(?P<ptype>(CONFIRM|ACK|COMMIT|DONE|LIST))'#ignore LIST
re_id = '(?P<id>(\\d+))'
re_src = '(?P<srcip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
re_dst = '(?P<dstip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
re_timestamp2 = '(?P<tstamp2>(\\d+))'
re_datasize = '(?P<datasize>(\\d+))'
#re_list=

rg_header = re.compile('log: node=(?P<nodeid>\d+) ip=(?P<nodeip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
    '( \((?P<rootstat>(ROOT NODE))\))?')
rg_packet = re.compile(re_timestamp+re_separator+re_action+re_separator+re_type+re_separator+re_id+re_separator+re_src+re_separator+re_dst+re_separator+re_timestamp2+re_separator+re_datasize,re.IGNORECASE|re.DOTALL)
rg_exec = re.compile('^(?P<tstamp>\d+):\s+rollout (?P<endres>(executed|aborted))')

'''
txt = "1413728139487: sent LIST|0|10.100.0.2|10.100.0.255|1413728139487|['10.100.0.2', '10.100.0.5', '10.100.0.3', '10.100.0.1']|[100] to 10.100.0.255"
m = rg_packet.search(txt)
if m:
    timestamp=m.group('tstamp')
    action=m.group('action')
    message_type=m.group('ptype')
    message_id=m.group('id')
    src_ip=m.group('srcip')
    dst_ip=m.group('dstip')
    timestamp2=m.group('tstamp2')
    datasize=m.group('datasize')
    print "("+timestamp+")("+action+")("+message_type+")("+message_id+")("+src_ip+")("+dst_ip+")("+timestamp2+")("+datasize+")"
exit(0)
'''

for i in range(min_log, max_log + 1):
	log_filenames.append(str(log_prefix + str(i) + '.log'))

for filename in log_filenames:
	try:
		file = open(log_path + filename, 'r')
		if debug:
        		print 'loaded log file: ' + filename
		files.append(file.readlines())
		file.close()
	except IOError:
		print 'WARNING: could not find log file: ' + filename
		pass

# begin parsing log files
try:
	if not os.path.exists(output_path):
		os.mkdir(output_path)
		if debug:
        		print 'created output folder: ' + output_path
	else:
		if debug:
        		print 'output folder exists'
except OSError:
	print 'ERROR: could not create output folder: ' + output_path
	pass

packet_log = open(output_path + 'packets.csv', 'w')
if debug:
    print 'generating packets.csv'

detailed_log = open(output_path + 'details.csv', 'w')
detailed_log.write("node id,node ip,root,confirm time,commit time,finish time,sent packets,received packets\n")
if debug:
    print 'generating details.csv'

for entry in files:
	#print 'parsing log: ' + str(entry)
	try:
		test_val = rg_header.search(entry[0])
	except:
		print 'ERROR parsing log: ' + str(entry)	
		continue#pass
	log_header = rg_header.search(entry[0])
	node_id = int(log_header.group('nodeid'))
	node_ip = log_header.group('nodeip')
	is_root = (log_header.group('rootstat') == 'ROOT NODE')

	confirm_time = 0
	commit_time = 0
	finish_time = 0
 
	if debug:
		print 'generating node' + str(node_id) + '.csv'
		print 'ROOT: ' + str(is_root)
    	node_log = open(output_path + 'node' + str(node_id) + '.csv', 'w')

	sent_packets = 0
	received_packets = 0

	for line in entry:
		#print line
		m = rg_packet.search(line)
		if m:
			timestamp = m.group('tstamp')
			action = m.group('action')
			message_type = m.group('ptype')
			message_id = m.group('id')
			src_ip = m.group('srcip')
			dst_ip = m.group('dstip')
			timestamp2 = m.group('tstamp2')
			datasize = m.group('datasize')
			
			current_duration = int(timestamp)
			if current_duration > 0 and message_type != 'LIST':#TODO: check
				if current_duration < lowest_duration or lowest_duration == 0:
					lowest_duration = current_duration

			if message_type == "CONFIRM":
				if current_duration < confirm_time or confirm_time == 0:
					confirm_time = current_duration
#				#get first message time
#				if current_duration < lowest_duration or lowest_duration == 0:
#					lowest_duration = current_duration
     			if message_type == "COMMIT":
				if current_duration < commit_time or commit_time == 0:
					commit_time = current_duration
     
		
			was_sent = (action == 'sent')
			was_received = (action == 'received')
			delay = int(timestamp) - int(timestamp2)
			if debug:
        			print timestamp + ':' + action + ' ' + message_type + ' ' + src_ip + ' to ' + dst_ip + ' @ ' + timestamp2 + '[' + datasize + 'bytes] [' + str(delay) + ']'

			if was_sent:#TODO: check this condition
				type_counts[message_type] += 1
				total_messages += 1
				sent_packets += 1
				node_log.write(timestamp2 + ',' + message_type + ',' + dst_ip + ',' + datasize + '\n')
				packet_log.write(timestamp2 + ',' + message_type + ',' + src_ip + ',' + dst_ip + ',' + datasize +  '\n')

			if was_received:
				received_packets += 1

		z = rg_exec.search(line)
		if z:
			#get last executed time
			finish_time = int(z.group('tstamp'))
			if finish_time > highest_duration:
				highest_duration = finish_time    
			if z.group('endres') == "executed":
				exec_count += 1
				if confirm_time == 0:#unconditional
					exec_nodes.append((node_id,commit_time,finish_time))
				else:#conditional
					exec_nodes.append((node_id,confirm_time,finish_time))    
			if z.group('endres') == "aborted":
				abort_count += 1
				abort_nodes.append((node_id,confirm_time,finish_time))

	node_log.close()
	if debug:
        	print 'lowest: ' + str(lowest_duration) + ', highest: ' + str(highest_duration)
 
	if debug:
        	print 'generating sentreceived' + str(node_id) + '.csv'
	sent_received = open(output_path + 'sentreceived' + str(node_id) + '.csv','w')
	sent_received.write(str(sent_packets) + ',' + str(received_packets) + '\n')
	sent_received.close()
 
	node_sent_packets.append((node_id,sent_packets))
 
	detailed_log.write(str(node_id) + ',' + str(node_ip) + ',' + str(is_root) + ',' + str(confirm_time) + ',' + str(commit_time) + ',' + str(finish_time) + ',' + str(sent_packets) + ',' + str(received_packets) + '\n')
	
detailed_log.close()
			
packet_log.close()

if debug:
    print 'generating duration.txt'
duration = open(output_path + 'duration.txt', 'w')
duration.write(str(highest_duration - lowest_duration) + '\n')#TODO: check this
duration.close()

for entry in exec_nodes:
	(node_id, comm_time, exec_time) = entry
	exec_times.append(str(node_id) + ',' + str(exec_time - comm_time))#lowest_duration # + '\n')
for entry in abort_nodes:
	(node_id, comm_time, abort_time) = entry
	abort_times.append(str(node_id) + ',' + str(exec_time - abort_time))#lowest_duration # + '\n')

if debug:
    print 'generating duration_node.txt'
duration_node = open(output_path + 'duration_node.txt', 'w')
for entry in exec_times:
    duration_node.write(entry.split(',')[1] + '\n')
duration_node.close()

if debug:
    print 'generating exectimes.csv'
exec_log = open(output_path + 'exectimes.csv', 'w')
for entry in exec_times:
	exec_log.write(entry + '\n')
exec_log.close()
if debug:
    print 'generating aborttimes.csv'
abort_log = open(output_path + 'aborttimes.csv', 'w')
for entry in abort_times:
	abort_log.write(entry + '\n')
abort_log.close()

if debug:
    print 'generating execcount.txt'
execs = open(output_path + 'execcount.txt', 'w')
execs.write(str(exec_count) + '\n')
execs.close()
if debug:
    print 'generating abortcount.txt'
aborts = open(output_path + 'abortcount.txt', 'w')
aborts.write(str(abort_count) + '\n')
aborts.close()

if debug:
    print 'generating totalpackets.txt'
total_packets = open(output_path + 'totalpackets.txt', 'w')
total_packets.write(str(total_messages) + '\n')
total_packets.close()
if debug:
    print 'generating totalpackets_node.txt'
total_packets_node = open(output_path + 'totalpackets_node.txt', 'w')
for entry in node_sent_packets:
    total_packets_node.write(str(entry[1]) + '\n')
total_packets_node.close()

if debug:
    print 'generating typecounts.csv'
typecounts = open(output_path + 'typecounts.csv', 'w')
for entry in type_counts:
	typecounts.write(entry + ',' + str(type_counts[entry]) + '\n')
typecounts.close()

print 'finished processing logs'
